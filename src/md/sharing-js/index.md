---
title: "sharing js"
shared-js:
  - choo
  - bel
js:
  - app
---

Let's say you want to use `jQuery`. Chances are you want a bunch of files to use
`jQuery` (or every JavaScript file you write will you `jQuery`). In this case
you have a "global" JavaScript file, which we enable like this:

```yaml
---
shared-js:
  - jquery
---
```

For this to work, you need to download jquery and put it into the `src/js`
directory, like so:

```bash
# this assumes you are at the root of the project
curl https://code.jquery.com/jquery-3.2.1.js -o src/js/jquery.js
```

We've used `choojs` in this page with the following config:

```yaml
---
title: "sharing js"
shared-js:
  - choo
  - bel
local-js:
  - app
---
```

This is what our js file (`app.js`) looks like:

```js
var html = bel

var app = choo()
app.use(countStore)
app.route('*', mainView)
app.mount('#hello')

function mainView (state, emit) {
  return html`
    <div>
      <h1>count is ${state.count}</h1>
      <button onclick=${onclick}>Increment</button>
    </div>
  `

  function onclick () {
    emit('increment', 1)
  }
}

function countStore (state, emitter) {
  state.count = 0
  emitter.on('increment', function (count) {
    state.count += count
    emitter.emit('render')
  })
  }
```


<div id="hello"></div>
