var html = bel
var app = choo()

app.use(countStore)
app.route('*', mainView)
app.mount('#hello')

function mainView(state, emit) {
  return html`
    <div>
      <h1>count is ${state.count}</h1>
      <button onclick=${onclick}>Increment</button>
    </div>
  `

  function onclick() {
    emit('increment', 1)
  }
}

function countStore(state, emitter) {
  state.count = 0
  emitter.on('increment', function(count) {
    state.count += count
    emitter.emit('render')
  })
}
