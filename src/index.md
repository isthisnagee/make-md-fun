---
title: Hello World
---

Hi all. This static site generator takes markdown (`.md`) files and converts
them to HTML (`.html`) files, and allows an easy way to include some custom JS.

Let's take a look at a few examples:

- [Sharing JS files:](sharing-js/index.html)

