# use `:=` instead of `=` to expand right away,
# see https://www.gnu.org/software/make/manual/make.html#Flavors
MD_SOURCE:=$(wildcard src/md/**/*.md)
CSS_SOURCE:=$(wildcard src/css/*.css)
JS_SOURCE:=$(wildcard src/js/*.js)
HTML_SOURCE:=$(MD_SOURCE:.md=.html)

HTML_FILES:=$(HTML_SOURCE:src/md/%=dist/%)
CSS_FILES:=$(CSS_SOURCE:src/css/%=dist/css/%)
JS_FILES:=$(JS_SOURCE:src/js/%=dist/js/%)

TEMPLATE=--template=templates/_layout
PANDOC_TYPE=-t html5+fenced_code_attributes+fenced_code_blocks
# CSS=--css=css/main.css
# LATEX=--katex

# src/md/*.md -> dist/*.html
all: init dist/index.html $(HTML_FILES) $(JS_FILES) $(HTML_FILES) 

dist/index.html: src/index.md
	pandoc -s $(TEMPLATE) $(PANDOC_TYPE) -o $@ $<

dist/%/index.html: src/md/%/index.md
	# copy all of the non `.md` files from src/md/{{dirname}} into dist/{{dirname}}
	rsync -a $(dir $<)* $(dir $@) --exclude=*.md 
	# generate dist/{{dirname}}/index.html using a template that recognizes js
	# files :)
	pandoc -s $(TEMPLATE) -t html5 -o $@ $<

dist/css/%.css: src/css/%.css
	cp -r $< $@

dist/js/%.js: src/js/%.js
	cp -r $< $@



# # push to gh-pages
# online:
# 	make clean && make
# 	git add .
# 	git commit -m 'Updated Docs'
# 	git push

clean:
	rm -rf dist/*

init:
	# check pandoc is installed
	command -v pandoc >/dev/null 2>&1 || echo "Please install pandoc"
	mkdir -p dist
	mkdir -p dist/js
	mkdir -p dist/css
