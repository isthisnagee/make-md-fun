> WIP

# make-md-fun

This is a very basic static site generator that takes your markdown files,
parses them using pandoc, and enables some JavaScript usage in those markdown
(or rather, html) files.


## Structure

```
.
├── Makefile
├── README.md
├── dist # the generated output of `src`
├── src # what you edit
│   ├── css # CSS files that _can_ be shared across markdown files 
│   ├── index.md # root file, output is `index.html`
│   ├── js # JavaScript files that _can_ be shared across markdown files 
│   └── md # everything under this should be a directory
│       └── sharing-js
│           └── index.md # `make` will generate dist/sharing-js/index.html`
│           └── action.js # index.md can reference this file
└── templates
    └── _layout.html5
```
